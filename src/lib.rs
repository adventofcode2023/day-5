use std::fs::File;
use std::io::{self, prelude::*};
use std::path::PathBuf;

pub mod disjoint_range;
pub mod parser;
pub mod range_map;
#[derive(Debug, Default)]
pub enum FileToParse {
    #[default]
    Input,
    Test(u64),
}

impl ToString for FileToParse {
    fn to_string(&self) -> String {
        match self {
            FileToParse::Input => String::from("input.txt"),
            FileToParse::Test(n) => format!("test-{n}.txt"),
        }
    }
}

impl FileToParse {
    pub fn path(&self) -> PathBuf {
        PathBuf::from(self.to_string())
    }
}

pub fn get(target: FileToParse) -> Result<String, std::io::Error> {
    let mut file = File::open(target.path())?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;
    Ok(input)
}

pub fn get_lines(target: FileToParse) -> Result<impl Iterator<Item = String>, std::io::Error> {
    let file = File::open(target.path())?;

    Ok(io::BufReader::new(file).lines().map(Result::unwrap))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn filename_from_enum() {
        println!("{}", FileToParse::Input.to_string());
        println!("{}", FileToParse::Test(1).to_string());
    }
}
