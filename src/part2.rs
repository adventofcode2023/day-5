#![allow(unused)]

use if_you_give_a_seed_a_fertilizer::disjoint_range::DisjointRange;
use if_you_give_a_seed_a_fertilizer::get_lines;
use if_you_give_a_seed_a_fertilizer::parser::{full, IDMap};
use if_you_give_a_seed_a_fertilizer::FileToParse::Input;
use rayon::iter::{IntoParallelIterator, IntoParallelRefIterator, ParallelIterator};

fn process(input: &str) -> u64 {
    let (_, (seeds, listings)) = full(input).expect("Parse successful.");

    let condensed_map = listings
        .into_iter()
        .map(|idmap| idmap.mappings)
        .reduce(|lhs, rhs| lhs.concatenate(&rhs))
        .expect("Condense map should not fail.");

    // expand seeds into ranges
    let mut seed_ranges: Vec<DisjointRange> = seeds
        .chunks(2)
        .map(|pair| (pair[0]..=(pair[0] + pair[1] - 1)).into())
        .collect();
    seed_ranges.sort();

    // find the ranges that will transform into lowest location in the condensed map.
    let mut seed_domains: Vec<DisjointRange> = seed_ranges
        .iter()
        .flat_map(|range| condensed_map.map_range(*range))
        .collect();

    seed_domains.sort();

    seed_domains[0].start
}

fn main() -> std::io::Result<()> {
    let result = process(include_str!("../input.txt"));

    println!("{result}");

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use if_you_give_a_seed_a_fertilizer::FileToParse::Test;
    use rstest::rstest;

    #[rstest]
    #[case(79, 81)]
    #[case(14, 14)]
    #[case(55, 57)]
    #[case(13, 13)]
    fn test_mapping(#[case] seed: u64, #[case] soil: u64) {
        let fixture = include_str!("../test-1.txt");
        let all = full(fixture);
    }

    #[test]
    fn test_process() {
        assert_eq!(46, process(include_str!("../test-1.txt")));
    }
}
