use if_you_give_a_seed_a_fertilizer::parser::full;

fn process(input: &str) -> u64 {
    let (_, (seeds, listings)) = full(input).expect("Parse successful.");

    let condensed_map = listings
        .into_iter()
        .map(|idmap| idmap.mappings)
        .reduce(|lhs, rhs| lhs.concatenate(&rhs))
        .expect("Condense map should not fail.");

    seeds
        .into_iter()
        .map(|seed| condensed_map.map(seed))
        .min()
        .unwrap()
}

fn main() -> std::io::Result<()> {
    let result = process(include_str!("../input.txt"));

    println!("{result}");

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::rstest;

    #[rstest]
    #[case(79, 82)]
    #[case(14, 43)]
    #[case(55, 86)]
    #[case(13, 35)]
    fn test_mapping(#[case] seed: u64, #[case] location: u64) {
        let fixture = include_str!("../test-1.txt");

        let (_, (_, listings)) = full(fixture).expect("Parse successful.");

        let condensed_map = listings
            .into_iter()
            .map(|idmap| idmap.mappings)
            .reduce(|lhs, rhs| lhs.concatenate(&rhs))
            .expect("Condense map should not fail.");

        assert_eq!(condensed_map.map(seed), location);
    }

    #[test]
    fn test_process() {
        assert_eq!(35, process(include_str!("../test-1.txt")));
    }
}
